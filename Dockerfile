# SPDX-FileCopyrightText: 2024 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-FileCopyrightText: 2023 Dataport AöR
# SPDX-License-Identifier: EUPL-1.2
FROM docker.io/python:3.12.2-bookworm@sha256:3e7c0f87d7c085a6ec7511b2fa69194b21ea54bc3a110c35e37ba20cbd0aef7e AS build

SHELL ["/bin/bash", "-o", "pipefail", "-c"]

ARG APPS_DOWNLOAD_URL_SCHEME
ARG APPS_DOWNLOAD_USER
ARG APPS_DOWNLOAD_PASSWORD
ARG NEXTCLOUD_DOWNLOAD_URL_SCHEME
ARG NEXTCLOUD_DOWNLOAD_USER
ARG NEXTCLOUD_DOWNLOAD_PASSWORD
ARG NEXTCLOUD_APPS_REMOVE="dashboard,\
                           firstrunwizard,\
                           logreader,\
                           nextcloud_announcements,\
                           password_policy,\
                           photos,\
                           related_resources,\
                           survey_client,\
                           updatenotification,\
                           user_saml,\
                           user_status,\
                           weather_status"
ARG NEXTCLOUD_IGNORE_CURRENT_VERSION="true"

COPY ./src/scripts/build \
     ./src/scripts/runtime \
     ./src/keys \
     ./src/files/apps.yaml \
     /build/
WORKDIR /build/
RUN pip install --no-cache-dir requests==2.31.0 cachetools==5.3.2 pyyaml==6.0.1 \
 && python3 -u /build/download_nextcloud_and_apps.py

FROM registry.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-php:1.12.3@sha256:72e574b5862bb0bd6798754931bc9a5d1092d802c14cb69e40fa5f3b23ba9674
LABEL org.opencontainers.image.authors="Bundesministerium des Innern und für Heimat" \
      org.opencontainers.image.documentation=https://gitlab.opencode.de:bmi/opendesk/components/platform-development/images/opendesk-nextcloud-management/-/blob/main/README.md \
      org.opencontainers.image.source=https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-management \
      org.opencontainers.image.vendor="Bundesministerium des Innern und für Heimat" \
      org.opencontainers.image.licenses=EUPL-1.2

COPY --chown=65532:65532 --from=build /build/output/ /
