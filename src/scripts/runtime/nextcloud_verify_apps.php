// SPDX-FileCopyrightText: 2023 Dataport AöR
// SPDX-License-Identifier: EUPL-1.2
<?php

// integration_swp does not contain signatures as its not an official app on the app store
$ignore_apps = ["integration_swp", "integration_phoenix"];


// returns major version of version string
function get_major_version($version) {
    $version = trim($version);
    $parts = explode(".", $version);

    // when an error occurs
    if (count($parts) < 1) {
        print("get_major_version: could not parse version string. Input was: $version" . PHP_EOL);
        return null;
    }

    return $parts[0];
}

function get_versions() {
    if (!file_exists("/var/www/versions.json")) {
        print("File not found.");
        exit(1);
    }
    $raw = file_get_contents("/var/www/versions.json");
    $data = json_decode($raw, true);
    return $data;
}

function check_app($basedir, $app_name) {
    print("checking $app_name in $basedir..." . PHP_EOL);
    $current_dir = getcwd();
    chdir($basedir);

    $command = "php occ integrity:check-app $app_name";
    $output = [];
    $code = 1;

    $result = exec($command, $output, $code);

    if ($result === false || $code != 0) {
        print("Error on integrity-check." . PHP_EOL);
        print("exit-code: $code, output:" . PHP_EOL);
        print(implode(PHP_EOL, $output));
        exit(1);
    }

    chdir($current_dir);
    print("Done." . PHP_EOL);
}

function check_apps_in_version($nc_version, $apps, $main_version = true) {
    global $ignore_apps;
    if (!$main_version) {
        print("Extracting nextcloud... ");
        $major = get_major_version($nc_version);
        $dir = "/var/nextcloud/versions/$major";
        $zipfile = "/var/nextcloud/versions/$major.zip";

        mkdir($dir);
        $zip = new ZipArchive;
        $res = $zip->open($zipfile);
        if ($res === TRUE) {
            $zip->extractTo($dir);
            $zip->close();
            print("Done." . PHP_EOL);
        } else {
            print("Extraction failed." . PHP_EOL);
            exit(1);
        }
    } else {
        $dir = "/var/www/html";
    }

    foreach ($apps as $app_name => $app_version) {
        if (in_array($app_name, $ignore_apps)) {
            print("Skipping $app_name." . PHP_EOL);
        } else {
            check_app($dir, $app_name);
        }
        print(PHP_EOL);
    }
}


$versions = get_versions();

$main_version = true;
foreach ($versions as $nc_version => $apps) {
    check_apps_in_version($nc_version, $apps, $main_version);

    $main_version = false;
}
