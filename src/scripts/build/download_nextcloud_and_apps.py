# SPDX-FileCopyrightText: 2024 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-FileCopyrightText: 2023 Dataport AöR
# SPDX-License-Identifier: EUPL-1.2
import requests
import json
import os
import re
import shutil
import hashlib
import subprocess
import yaml

from pathlib import Path
from requests.auth import HTTPBasicAuth
from cachetools import cached

# TODO: nextcloud version: add @ between nc and version number

print(os.environ)

NEXTCLOUD_APPS_REMOVE = os.getenv('NEXTCLOUD_APPS_REMOVE', '')
NEXTCLOUD_IGNORE_CURRENT_VERSION = os.getenv("NEXTCLOUD_IGNORE_CURRENT_VERSION", "false").lower()

APPS_DOWNLOAD_URL_SCHEME = os.getenv('APPS_DOWNLOAD_URL_SCHEME')
APPS_DOWNLOAD_USER = os.getenv('APPS_DOWNLOAD_USER')
APPS_DOWNLOAD_PASSWORD = os.getenv('APPS_DOWNLOAD_PASSWORD')

NEXTCLOUD_DOWNLOAD_URL_SCHEME = os.getenv('NEXTCLOUD_DOWNLOAD_URL_SCHEME')
NEXTCLOUD_DOWNLOAD_USER = os.getenv('NEXTCLOUD_DOWNLOAD_USER')
NEXTCLOUD_DOWNLOAD_PASSWORD = os.getenv('NEXTCLOUD_DOWNLOAD_PASSWORD')

APPS_FROM_GITHUB = os.getenv("APPS_DOWNLOAD_FROM_GITHUB", "integration_swp,integration_phoenix")


# loads the apps info from the app store api for a specific version
@cached(cache = {})
def get_apps_json(release):
    version_parts = release.split(".")
    version_parts = version_parts[:3]
    version_string = ".".join(version_parts)
    print(version_string)

    url = "https://apps.nextcloud.com/api/v1/platform/" + version_string + "/apps.json"
    response = requests.get(url)
    if response.status_code != 200:
        print("Cant get apps for nc version", release)
        exit(1)

    json_raw = response.text
    data = json.loads(json_raw)

    return data


# gets the information for a specific app
def get_app_data(app_name, nextcloud_release):
    for app in get_apps_json(nextcloud_release):
        if app["id"] == app_name:
            return app
    return None


# checks if app release is rc or not
def is_non_release_version(version):
    if re.match(r".+-rc.+", version.lower()):
        return True
    if re.match(r".+-alpha.+", version.lower()):
        return True
    if re.match(r".+-beta.+", version.lower()):
        return True

    return False


# returns latest version and download url for an app
def get_app_latest_version(app_name, nextcloud_release):
    data = get_app_data(app_name, nextcloud_release)
    if data is None:
        return None, None
    releases = data["releases"]
    for release in releases:
        version = release["version"]
        if is_non_release_version(version):
            continue
        url = release["download"]
        return version, url
    return None, None


def get_app_download_url_from_github(app_name, app_release):
    api_url = f"https://api.github.com/repos/nextcloud/{app_name}/releases/tags/v{app_release}"
    response = requests.get(api_url)
    if response.status_code != 200:
        print(f"Cant get {app_name} v{app_release} from github")
        exit(1)

    json_raw = response.text
    data = json.loads(json_raw)

    download_url = data["assets"][0]["browser_download_url"]
    return download_url


@cached(cache = {})
def get_app_names_to_pull_from_github():
    app_names = APPS_FROM_GITHUB.strip(",").split(",")
    return app_names


def get_app_download_url(app_name, nextcloud_release, app_release):
    if APPS_DOWNLOAD_URL_SCHEME is not None and APPS_DOWNLOAD_URL_SCHEME != "":
        print(f"Downloading App {app_name} from private repo")
        url = APPS_DOWNLOAD_URL_SCHEME.replace("{{ APP_VERSION }}", app_release).replace("{{ APP_NAME }}", app_name)
        url_sha256 = url + ".sha256"
        return url, url_sha256

    if app_name in get_app_names_to_pull_from_github():
        return get_app_download_url_from_github(app_name, app_release), None

    return get_app_download_url_store(app_name, nextcloud_release, app_release), None

# returns the url to the specified app version
def get_app_download_url_store(app_name, nextcloud_release, app_release):
    data = get_app_data(app_name, nextcloud_release)
    if data is None:
        print("Could not download" + app_name +", version " + app_release)
        exit(1)

    releases = data["releases"]
    for release in releases:
        if release["version"] == app_release:
            return release["download"]

    print("Version " + str(app_release) + " of app " + app_name + " not found.")
    exit(1)


def generate_target_filename(app_name, app_version):
    return "nextcloud_app_" + app_name + "_" + app_version + ".tar.gz"


# downloads file from url and saves it to specified filename
def download_file(url, filename, username = None, password = None):
    print("Downloading", url, "to", filename)
    # file already downloaded
    if os.path.exists(filename):
        return

    auth = None
    if username is not None and password is not None:
        auth = HTTPBasicAuth(username, password)

    response = requests.get(url, auth=auth, allow_redirects=True)

    if response.status_code != 200:
        print("Downloading from", url, "failed.")
        exit(1)

    content = response.content
    with open(filename, "wb") as fh:
        fh.write(content)


# generates the hash of a file and compares it to a given value
def verify_file_checksum(filename, checksum):
    file_content = Path(filename).read_bytes()
    file_hash = hashlib.sha256(file_content)

    if file_hash.hexdigest() != checksum:
        print(filename + ": Checksum error!")
        exit(1)
    return


# extracts the hash string from a sha256sum file
def read_checksum_file(checksum_filename):
    with open(checksum_filename) as f:
        lines = f.readlines()

    if len(lines) < 1:
        print(checksum_filename + ": Invalid checksum file")
        exit(1)

    checksums = re.findall(r'^\w+', lines[0])
    if (len(checksums) < 1):
        print(checksum_filename + ": Invalid checksum file")
        exit(1)
    return checksums[0]


def get_nextcloud_download_url(nextcloud_release):


    if NEXTCLOUD_DOWNLOAD_URL_SCHEME is not None and NEXTCLOUD_DOWNLOAD_URL_SCHEME != "":
        url = NEXTCLOUD_DOWNLOAD_URL_SCHEME.replace("{{ NEXTCLOUD_VERSION }}", nextcloud_release)
        url_sha256 = url + ".sha256sum"
    else:
        # nextcloud versions in the url have only three parts
        parts = nextcloud_release.split(".")
        nextcloud_release = ".".join(parts[:3])
        url = "https://download.nextcloud.com/server/releases/nextcloud-" + nextcloud_release + ".zip"
        url_sha256 = url + ".sha256"

    url_asc = url + ".asc"

    return url, url_sha256, url_asc



def download_and_extract_nextcloud(nextcloud_release, target_dir):
    url, url_sha256, url_asc = get_nextcloud_download_url(nextcloud_release)

    print("Downloading Nextcloud " + nextcloud_release + "...")
    tmp_filename_nc = "tmp/nextcloud-" + nextcloud_release + ".zip"
    tmp_filename_nc_hash = tmp_filename_nc + ".sha256sum"
    tmp_filename_nc_checksum = tmp_filename_nc + ".asc"

    download_user = None
    download_password = None

    if (NEXTCLOUD_DOWNLOAD_USER is not None and
            NEXTCLOUD_DOWNLOAD_PASSWORD is not None and
            NEXTCLOUD_DOWNLOAD_USER != "" and
            NEXTCLOUD_DOWNLOAD_PASSWORD != ""):
        download_user = NEXTCLOUD_DOWNLOAD_USER
        download_password = NEXTCLOUD_DOWNLOAD_PASSWORD


    download_file(url, tmp_filename_nc, download_user, download_password)
    download_file(url_sha256, tmp_filename_nc_hash, download_user, download_password)
    download_file(url_asc, tmp_filename_nc_checksum, download_user, download_password)

    print("Checking Hash...")
    checksum = read_checksum_file(tmp_filename_nc_hash)
    verify_file_checksum(tmp_filename_nc, checksum)

    print("Checking Checksum...")
    result = subprocess.run(['/bin/bash', '/build/nextcloud_verify.sh', '/build/nextcloud.asc', tmp_filename_nc], stdout=subprocess.PIPE)
    if result.returncode != 0:
        print("Checksum verification failed. Output:")
        print(result.stdout.decode())
        exit(1)

    target_dir_parts = os.path.abspath(os.path.join(target_dir, os.pardir))
    os.makedirs(target_dir_parts, exist_ok=True)

    print("Extracting...")
    shutil.unpack_archive(tmp_filename_nc)
    shutil.move("nextcloud/", target_dir)

    os.remove(tmp_filename_nc)
    os.remove(tmp_filename_nc_hash)
    print("Download done.")


def get_target_dir(nextcloud_release, is_main_runtime_version = False):
    if is_main_runtime_version:
        target_dir = "output/var/www/html/"
    else:
        major_version = nextcloud_release.split(".")[0]
        target_dir = "output/var/nextcloud/versions/" + str(major_version) + "/"

    return target_dir


def write_version_info_file(nextcloud_release, apps, target_file = "output/var/www/nextcloud_version.txt"):
    output = ["nextcloud_version=\"" + nextcloud_release + "\""]
    for app_name, app_version in apps.items():
        output.append("nextcloud_app_" + app_name + "_version=\"" + app_version + "\"")

    output_string = ", ".join(output)

    with open(target_file, "w") as fh:
        fh.write(output_string)

    print("Version info written to", target_file)



# main loop for a nextcloud release
def prepare_nextcloud_with_apps(nextcloud_release, apps, is_main_runtime_version = False, remove_apps = []):

    target_dir = get_target_dir(nextcloud_release, is_main_runtime_version)
    download_and_extract_nextcloud(nextcloud_release, target_dir)
    os.makedirs(target_dir, exist_ok=True)

    download_user = None
    download_password = None

    if (APPS_DOWNLOAD_USER is not None and
            APPS_DOWNLOAD_PASSWORD is not None and
            APPS_DOWNLOAD_USER != "" and
            APPS_DOWNLOAD_PASSWORD != ""):
        download_user = APPS_DOWNLOAD_USER
        download_password = APPS_DOWNLOAD_PASSWORD

    print("Downloading apps...")
    for app_name, app_version in apps.items():

        print("Downloading", app_name, "in version", str(app_version))

        # get version, download url and expected filename
        download_url, download_url_sha256 = get_app_download_url(app_name, nextcloud_release, app_version)
        target_filename = os.path.join("tmp/", generate_target_filename(app_name, app_version))
        target_filename_sha256 = target_filename + ".sha256sum"

        # download app
        download_file(download_url, target_filename, download_user, download_password)
        if download_url_sha256 is not None:
            download_file(download_url_sha256, target_filename_sha256, download_user, download_password)
            print("Checking Hashes...")
            verify_file_checksum(target_filename, read_checksum_file(target_filename_sha256))

        print("Extracting...")
        shutil.unpack_archive(target_filename, os.path.join(target_dir, "apps/"))


        print("Done.")
        print("")

    for app in remove_apps:
        app_path = os.path.join(target_dir, "apps", app)
        if not os.path.exists(app_path):
            print("App", app, "could not be removed, path doesnt exist")
        else:
            print("Removing App", app)
            shutil.rmtree(app_path)

    if not is_main_runtime_version:
        print("Compressing nextcloud directory...")
        shutil.make_archive(target_dir.rstrip("/"), "zip", target_dir)
        shutil.rmtree(target_dir)

    if is_main_runtime_version:
        write_version_info_file(nextcloud_release, apps)

    print("Done with Nextcloud", nextcloud_release)
    print("")

nextcloud_apps_remove_string = NEXTCLOUD_APPS_REMOVE.lower().replace("\t", "").replace("\n", "").replace(" ", "").strip(",")
nextcloud_apps_remove = nextcloud_apps_remove_string.split(",")

with open("/build/apps.yaml", "r") as file:
    nc_versions = yaml.safe_load(file)

print("Input was:")
print(json.dumps(nc_versions, indent=4))

# cleanup primarily for debugging
remove_dirs = ["output/", "nextcloud/"]
for d in remove_dirs:
    if os.path.exists(d):
        shutil.rmtree(d)

os.makedirs("tmp/", exist_ok=True)

for nc_version, apps in nc_versions.items():
    main_version = nc_version == list(nc_versions.keys())[0]
    if main_version and NEXTCLOUD_IGNORE_CURRENT_VERSION == "true":
        print("Skipping current version to save space")
        continue
    prepare_nextcloud_with_apps(nc_version, apps, is_main_runtime_version=main_version, remove_apps=nextcloud_apps_remove)

# write parsed version information into the container for later use
if NEXTCLOUD_IGNORE_CURRENT_VERSION == "true":
    os.makedirs("output/var/www/", exist_ok=True)
    with open("output/var/www/versions.json", "w") as fp:
        json.dump(nc_versions, fp, indent=4)
