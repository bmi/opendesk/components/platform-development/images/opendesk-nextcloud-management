#!/bin/bash
# SPDX-FileCopyrightText: 2023 Dataport AöR
# SPDX-License-Identifier: EUPL-1.2
gpg --import $1
gpg --verify $2.asc $2

TEST_RESULT=$?

if [ $TEST_RESULT -ne 0 ]; then
    echo "Signature not correct!"
    exit 1
fi
