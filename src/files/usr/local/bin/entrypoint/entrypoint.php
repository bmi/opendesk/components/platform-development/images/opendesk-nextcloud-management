<?php
// SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
// SPDX-FileCopyrightText: 2023 Dataport AöR
// SPDX-License-Identifier: EUPL-1.2

print(__FILE__ . PHP_EOL);
require_once "config_exit_codes.php";

// set some vars
define("NC_WWW_DIR", "/var/www/html");
define("NC_WWW_VERSIONS_DIR", "/var/nextcloud/versions");
define("NC_MOUNT_DIR", "/var/nextcloud/data");
define("NC_DATA_DIR", "/var/nextcloud/data/nextcloud_data");
define("NC_FONT_CACHE_DIR", "/tmp/fontconfig_cache");
define("PHP_FPM_BIN", "/usr/sbin/php-fpm");
define("PHP_BIN", "/usr/bin/php");
define("PHP_FPM_POOL_CONFIG", "/etc/php/8.2/fpm/pool.d/www.conf");
define("PHP_REDIS_CONFIG", "/etc/php/8.2/mods-available/redis.ini");
define("DEV_MODE", getenv("FS_ENV_DEV_MODE") !== false || getenv("DEV_MODE") !== false);

// configuration
define("NC_OCC_FORCE_ALL", false); // forces every occ command to be run

// change to current script path dir
chdir(realpath(__DIR__));
print("Running as uid " . posix_geteuid() . PHP_EOL);
if (DEV_MODE) {
    print("Dev-Mode enabled." . PHP_EOL);
}

$run_mode = "combined";
if (getenv("FS_ENV_RUN_MODE") !== false) {
    $run_mode = getenv("FS_ENV_RUN_MODE");
}

require_once "functions.php";
require_once "config_apps.php";
require_once "config.php";

define("CREATE_OCDATA_FILE", str2bool(getenv("FS_ENV_CREATE_OCDATA", false)));
define("UID", intval(get_from_env("FS_ENV_UID", 65532)));

// version dependent apps
$nc_version = get_nc_version();
print("Image contains Nextcloud $nc_version" . PHP_EOL);

// check if a data volume is mounted
if (file_exists(NC_MOUNT_DIR . "/dummyfile")) {
    print(NC_MOUNT_DIR . " is not a mount. Exiting..." . PHP_EOL);
    exit(EXIT_CODE_DATA_DIR_NO_MOUNT);
}

// check if data dir exists, otherwise create
if (!is_dir(NC_DATA_DIR)) {
    $create_data_dir = mkdir(NC_DATA_DIR, 0770);
    if (!$create_data_dir) {
        print("FAIL. Could not create data directory" . PHP_EOL);
        exit(EXIT_CODE_DATA_DIR_NOT_WRITABLE);
    }
    print("Created data directory." . PHP_EOL);
}

// check if fontconfig_cache exists, otherwise create
if (!is_dir(NC_FONT_CACHE_DIR)) {
    $create_font_cache_dir = mkdir(NC_FONT_CACHE_DIR, 0770);
    if (!$create_font_cache_dir) {
        print("FAIL. Could not create fontconfig_cache directory" . PHP_EOL);
        exit(EXIT_CODE_DATA_DIR_NOT_WRITABLE);
    }
    print("Created fontconfig_cache directory." . PHP_EOL);
}

// test if data dir is writable
$temp_filename = NC_DATA_DIR . "/testfile-" . bin2hex(random_bytes(10));
$write_result = file_put_contents($temp_filename, time());
if ($write_result === false) {
    print("ERROR: Data dir is not writable." . PHP_EOL);
    exit(EXIT_CODE_DATA_DIR_NOT_WRITABLE);
}
unlink($temp_filename);

// create temp and log dir
print("Creating tmp and log dir" . PHP_EOL);
$dirs = ["tmp", "log"];
foreach ($dirs as $dir) {
    print("  - $dir:");
    if (!file_exists(NC_DATA_DIR . "/" . $dir)) {
        $ok = mkdir(NC_DATA_DIR . "/" . $dir);
        if (!$ok) {
            print("FAIL. Could not create directory" . PHP_EOL);
            exit(EXIT_CODE_DATA_DIR_NOT_WRITABLE);
        }
        print("Created." . PHP_EOL);
    }
    print("Exists." . PHP_EOL);
}

wait_for_db();

switch ($run_mode) {
    case "combined":
        require_once "entrypoint_init.php";
        require_once "entrypoint_runtime.php";
        break;
    case "init":
        require_once "entrypoint_init.php";
        break;
    case "cron":
        require_once "entrypoint_cron.php";
        break;
    case "runtime":
    case "run":
        require_once "entrypoint_runtime.php";
        break;
    default:
        print("Invalid RUN_MODE. Valid values: combined, init, cron, runtime, run" . PHP_EOL);
        print("combined runs both init and after that runtime, run and runtime are equal." . PHP_EOL);
        exit(1);
}
