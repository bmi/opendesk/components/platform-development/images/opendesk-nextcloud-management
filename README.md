<!--
SPDX-FileCopyrightText: 2024 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
SPDX-License-Identifier: Apache-2.0
-->

# openDesk Nextcloud images

The openDesk Nextcloud images are a set of depended images.

Please visit the [central documentation in the *Base* repository](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-base-layer/-/blob/main/README.md)
for more information.
