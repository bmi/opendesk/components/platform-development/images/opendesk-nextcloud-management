## [1.6.3](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-management/compare/v1.6.2...v1.6.3) (2024-09-05)


### Bug Fixes

* **richdocuments:** Update to 8.4.6, ([97d27d6](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-management/commit/97d27d6354dc1d309bfecfe79c8124aee50e73db))
* **richdocuments:** Update to 8.4.6, ([5699d42](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-management/commit/5699d42d7751bf2fffc535b3f810b45c064ffddb))

## [1.6.2](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-management/compare/v1.6.1...v1.6.2) (2024-09-04)


### Bug Fixes

* Update to 29.0.6 including matching app versions. ([1965e06](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-management/commit/1965e06ad3b7553529730ffd362344e04bd7bdde))

## [1.6.1](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-management/compare/v1.6.0...v1.6.1) (2024-09-02)


### Bug Fixes

* Bump php layer. ([7b3ca30](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-management/commit/7b3ca3098614018bb0e16a5670d43e0995ad58a3))

# [1.6.0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-management/compare/v1.5.3...v1.6.0) (2024-09-02)


### Features

* Bump php layer to 1.12.0 ([12428d0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-management/commit/12428d03a77fb38ea5420f58567bfd981f6168b4))

## [1.5.3](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-management/compare/v1.5.2...v1.5.3) (2024-08-23)


### Bug Fixes

* **richdocuments:** Typo. ([da0d830](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-management/commit/da0d830491fc909e3a86af7875ed631ccf622475))

## [1.5.2](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-management/compare/v1.5.1...v1.5.2) (2024-08-23)


### Bug Fixes

* **richdocuments:** OCC call for `activate-config`. ([32b66d9](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-management/commit/32b66d9723ccd3d5a67500eaf85cbc8462b5cc3f))

## [1.5.1](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-management/compare/v1.5.0...v1.5.1) (2024-08-21)


### Bug Fixes

* **sharing:** Update handling of share settings. ([522ad89](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-management/commit/522ad8937018702d40209126a6dabe422ca1e509))

# [1.5.0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-management/compare/v1.4.4...v1.5.0) (2024-08-21)


### Features

* Nextcloud 29.0.5. ([3bc2686](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-management/commit/3bc26865c0888009ad1c157ba02bcb83545a3ad4))

## [1.4.4](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-management/compare/v1.4.3...v1.4.4) (2024-07-24)


### Bug Fixes

* **misc:** Update openincryptpad to 0.3.5 and bump php base container. ([67e920f](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-management/commit/67e920ffd41aa3304df76baa66c2ec340875906a))

## [1.4.3](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-management/compare/v1.4.2...v1.4.3) (2024-07-16)


### Bug Fixes

* **wopi:** Allow internal WOPI URL to be set differently from external one. ([9458f5a](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-management/commit/9458f5aa80016ed220b6ea46e0a6fee29e1d99ff))

## [1.4.2](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-management/compare/v1.4.1...v1.4.2) (2024-07-16)


### Bug Fixes

* **oidc:** Bump opendesk-nextcloud-php image to disable `selfencoded_bearer_validation_audience_check` in config.php. ([a0d8beb](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-management/commit/a0d8beb572dd528bc49347289f3772a775f136ce))

## [1.4.1](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-management/compare/v1.4.0...v1.4.1) (2024-07-03)


### Bug Fixes

* Update to 28.0.7 including latest apps for 28. ([a61ae65](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-management/commit/a61ae653243a9647b9ac23a30f9e95221ed8c66c))

# [1.4.0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-management/compare/v1.3.12...v1.4.0) (2024-07-03)


### Features

* **php:** Update opendesk-nextcloud-php to allow custom values for *_retention_obligation. ([ce382cb](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-management/commit/ce382cb89268a040a58a5fb8fd6bd1a7a31b4c32))

## [1.3.12](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-management/compare/v1.3.11...v1.3.12) (2024-05-07)


### Bug Fixes

* **nextcloud:** Bump php-layer to 28.0.5 and latest 28-release apps ([7f93d92](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-management/commit/7f93d92fc066d777e28557107bf8b59ea12bf462))

## [1.3.11](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-management/compare/v1.3.10...v1.3.11) (2024-05-06)


### Bug Fixes

* **integration_swp:** Bump to 3.1.16 ([8f2a36a](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-management/commit/8f2a36ae9f637bb94e6c738484a12e89c03c2eee))

## [1.3.10](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-management/compare/v1.3.9...v1.3.10) (2024-04-02)


### Bug Fixes

* **core:** Bump to 28.0.4 ([375344e](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-management/commit/375344e502d43d114d982a83fe65011f19d45258))

## [1.3.9](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-management/compare/v1.3.8...v1.3.9) (2024-03-28)


### Bug Fixes

* **sharing:** Set default share name to __Shared_with_me__ for better visibility ([522aff8](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-management/commit/522aff85b0ba45f43e67632e64ee6007b9158442))

## [1.3.8](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-management/compare/v1.3.7...v1.3.8) (2024-03-26)


### Bug Fixes

* **core:** Bump to include NC 28.0.3 ([ec22de5](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-management/commit/ec22de5312bd748d6fe28d3f801e3d2e27de8f10))

## [1.3.7](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-management/compare/v1.3.6...v1.3.7) (2024-03-18)


### Bug Fixes

* **nextcloud-php:** Bump to 1.8.6 ([243915d](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-management/commit/243915daad59c6d403997e027cee72cf79e7a7b3))

## [1.3.6](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-management/compare/v1.3.5...v1.3.6) (2024-03-11)


### Bug Fixes

* **image:** Bump php layer ([5c6465c](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-management/commit/5c6465c0fc1ff5d7d3970f209a341305def78d52))

## [1.3.5](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-management/compare/v1.3.4...v1.3.5) (2024-02-27)


### Bug Fixes

* **apps:** Bump integration_swp to 3.15 and update opendesk-nextcloud-php to 1.8.4 ([b246b71](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-management/commit/b246b71b0949599c35ceb340dc4dff3f6cbf6c84))

## [1.3.4](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-management/compare/v1.3.3...v1.3.4) (2024-02-15)


### Bug Fixes

* **Dockerfile:** Update registry.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-php Docker tag to v1.8.3 ([eeb6b0a](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-management/commit/eeb6b0af3d4a711d7adbfe6e0f31a9d3c9ddb6cb))

## [1.3.3](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-management/compare/v1.3.2...v1.3.3) (2024-02-15)


### Bug Fixes

* **Dockerfile:** Remove variables and base label ([1f3d01a](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-management/commit/1f3d01ad15ec4571a350abcce9034fea40ba9b11))

## [1.3.2](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-management/compare/v1.3.1...v1.3.2) (2024-02-15)


### Bug Fixes

* **Dockerfile:** Update docker.io/python Docker tag to v3.12.2 ([9b8d84f](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-management/commit/9b8d84fe3c504750d0002c025794c09ae3f5bc42))

## [1.3.1](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-management/compare/v1.3.0...v1.3.1) (2024-02-14)


### Bug Fixes

* **apps:** Bump openincryptpad to 0.3.3 ([260f694](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-management/commit/260f694886b0464c0ac0d1bb4078d81a75330ad3))

# [1.3.0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-management/compare/v1.2.0...v1.3.0) (2024-02-13)


### Features

* **ldap:** Support for Admin role through custom group ([d950a0b](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-management/commit/d950a0b0275bdeb81a38640aad762575580da234))

# [1.2.0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-management/compare/v1.1.1...v1.2.0) (2024-02-04)


### Features

* Add Nextcloud 28 support ([5542f02](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-management/commit/5542f02ab85c6af03629cac89b71a710ab38467a))

## [1.1.1](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-management/compare/v1.1.0...v1.1.1) (2024-01-31)


### Bug Fixes

* **Dockerfile:** Update base to 1.7.1 ([dbb70f5](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-management/commit/dbb70f5437d4286e1e364b0700c490ec104cb0d9))

# [1.1.0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-management/compare/v1.0.12...v1.1.0) (2024-01-31)


### Features

* **Dockerfile:** Update base to v1.7.0 ([1ec0a7b](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-management/commit/1ec0a7b0a34f52859c37975c66e1746f61b1e65c))

## [1.0.12](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-management/compare/v1.0.11...v1.0.12) (2024-01-30)


### Bug Fixes

* **Dockerfile:** Use YAML file for apps ([a38367e](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-management/commit/a38367eefb2b62da7052e599f20ecbbd6b57c201))

## [1.0.11](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-management/compare/v1.0.10...v1.0.11) (2023-12-28)


### Bug Fixes

* **Dockerfile:** Image contains only upgrades from Nextcloud 26 ([5ba06fb](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-management/commit/5ba06fbb301144a8cb00a57558c79f710bc7adae))
* **Dockerfile:** Update nextcloud php to v1.4.0 ([28c81de](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/opendesk-nextcloud-management/commit/28c81dee4e09a4d314836e6eec887e7b12e2bb5b))

## [1.0.10](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-php-archive/compare/v1.0.9...v1.0.10) (2023-12-21)


### Bug Fixes

* **Dockerfile:** Update nextcloud php to v1.3.2 ([351494e](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-php-archive/commit/351494e104858359d904bfc4df7d1a93405a3c56))

## [1.0.9](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-php-archive/compare/v1.0.8...v1.0.9) (2023-12-21)


### Bug Fixes

* **Dockerfile:** Update nextcloud php to v1.3.1 ([8cad5fd](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-php-archive/commit/8cad5fd77a69d0db9c3e582b3a0372458a425832))

## [1.0.8](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-php-archive/compare/v1.0.7...v1.0.8) (2023-12-21)


### Bug Fixes

* **Dockerfile:** Update nextcloud php to v1.3.0 ([d17d17c](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-php-archive/commit/d17d17c4712c8e482607e144137f02800eb3fd7d))

## [1.0.7](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-php-archive/compare/v1.0.6...v1.0.7) (2023-12-21)


### Bug Fixes

* **Dockerfile:** Update nextcloud php to v1.2.0 ([29d674d](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-php-archive/commit/29d674d035c2b55fc968430964c6a124affc932e))

## [1.0.6](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-php-archive/compare/v1.0.5...v1.0.6) (2023-12-20)


### Bug Fixes

* **Dockerfile:** Update nextcloud php to v1.1.5 ([f7ef4bb](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-php-archive/commit/f7ef4bbbd65d2349233e59cc98b79dfc2c51c27b))

## [1.0.5](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-php-archive/compare/v1.0.4...v1.0.5) (2023-12-20)


### Bug Fixes

* **Dockerfile:** Update php layer ([5c7c932](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-php-archive/commit/5c7c932c6ce4053865585e0d6e8832e7c119db0b))

## [1.0.4](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-php-archive/compare/v1.0.3...v1.0.4) (2023-12-20)


### Bug Fixes

* **Dockerfile:** Change paths of nextcloud directories ([a483e1a](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-php-archive/commit/a483e1a650725867f9264f5a4709ea6ce244796b))

## [1.0.3](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-php-archive/compare/v1.0.2...v1.0.3) (2023-12-19)


### Bug Fixes

* **Dockerfile:** Use downloader script correct ([daafbb7](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-php-archive/commit/daafbb719dc1f82dcc9ee87c25894659fbfae63b))

## [1.0.2](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-php-archive/compare/v1.0.1...v1.0.2) (2023-12-19)


### Bug Fixes

* **Dockerfile:** Fix additional artifact layers path ([1a603cb](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-php-archive/commit/1a603cb8d50499a8b925595e2b321224f4ed66ec))

## [1.0.1](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-php-archive/compare/v1.0.0...v1.0.1) (2023-12-19)


### Bug Fixes

* **Dockerfile:** Update apps ([30161b7](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-php-archive/commit/30161b7729a987466b2f42a8b9f2938b96014643))

# 1.0.0 (2023-12-19)


### Features

* **opendesk-nextcloud-artifacts-archive:** Initial commit ([a8bf886](https://gitlab.opencode.de/bmi/opendesk/components/images/opendesk-nextcloud-artifacts-archive/commit/a8bf886c4a826829635da3748cb47c684134be50))
